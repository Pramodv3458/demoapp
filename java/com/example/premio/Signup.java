package com.example.premio;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.textfield.TextInputLayout;

public class Signup extends AppCompatActivity {
    TextInputLayout email, password, name, mob;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        mob = findViewById(R.id.mobile);
        name = findViewById(R.id.name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ActionBar actionBar;
        actionBar = getSupportActionBar();

        // Define ColorDrawable object and parse color
        // using parseColor method
        // with color hash code as its parameter
        ColorDrawable colorDrawable
                = new ColorDrawable(Color.parseColor("#FF5722"));

        // Set BackgroundDrawable
        actionBar.setBackgroundDrawable(colorDrawable);

    }

    public void signup(View view) {
        String n =name.getEditText().getText().toString();
        String e =email.getEditText().getText().toString();
        String p =password.getEditText().getText().toString();
        String m =mob.getEditText().getText().toString();
        if (n.isEmpty())
        {
            name.setError("Enter the name");
            name.requestFocus();
        }
        else if (e.isEmpty() && e.contains("@") && e.contains(".com"))
        {
            email.setError("Enter the valid email");
            email.requestFocus();
        }
        else if (p.isEmpty())
        {
            password.setError("Enter your Password");
        }
        else if(m.isEmpty())
        {
            mob.setError("Enter your mobile number");
            mob.requestFocus();
        }
        else{

            Intent intent = new Intent(this,Login.class);
            intent.putExtra("email",e);
            intent.putExtra("password",p);
            startActivity(intent);

        }
    }
}
